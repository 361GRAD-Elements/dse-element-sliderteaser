<?php

/**
 * 361GRAD Sliderteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_sliderteaser'] = ['Slider Teaser', 'Slider Teaser.'];

$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Unter-Überschrift', 'Hier können Sie eine Subheadline hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];
$GLOBALS['TL_LANG']['tl_content']['textcolor_legend']   = 'Textfarbeneinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_textcolor']  = ['Textfarbe', 'Bitte wählen Sie die Farbe des Textes'];
$GLOBALS['TL_LANG']['tl_content']['dse_textcolor-black']  = 'Schwarz';
$GLOBALS['TL_LANG']['tl_content']['dse_textcolor-white']  = 'Weiß';
$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle']  = ['CTA Button Titel', 'Der Linktitel wird auf Hover angezeigt'];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']   = ['CTA Button Link', 'Bitte geben Sie eine Webadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Hintergrundbild', 'Bitte wählen Sie eine Datei oder einen Ordner aus dem Dateiverzeichnis aus.'];
$GLOBALS['TL_LANG']['tl_content']['sliderteaser_legend']   = 'Teaser-Einstellungen';